@echo off
cd %~dp0
nuget.exe pack Gomez.Core.Ef.csproj -IncludeReferencedProjects -Properties Configuration=Release -OutputDirectory "D:\NuGetPackages"