﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Gomez.Core.Ef.Cache.Db.Containers.EntityFramework
{
    public class DbCacheEntry : IDbCacheEntry
    {
        [MaxLength(128)]
        [Key, Column(Order = 0)]
        public string ContainerId { get; set; }

        [Key, Column(Order = 1)]
        [MaxLength(384)]
        public string Key { get; set; }

        public string JsonValue { get; set; }

        public string AssemblyQualifiedName { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public DateTime? ExpireAt { get; set; }
    }
}
