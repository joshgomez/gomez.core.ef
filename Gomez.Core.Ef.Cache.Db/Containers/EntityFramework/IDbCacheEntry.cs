﻿using System;

namespace Gomez.Core.Ef.Cache.Db.Containers.EntityFramework
{
    public interface IDbCacheEntry
    {
        string Key { get; set; }
        string ContainerId { get; set; }
        string JsonValue { get; set; }

        string AssemblyQualifiedName { get; set; }

        DateTime CreatedAt { get; set; }
        DateTime? UpdatedAt { get; set; }
        DateTime? ExpireAt { get; set; }
    }
}
