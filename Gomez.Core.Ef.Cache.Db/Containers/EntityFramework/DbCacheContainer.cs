﻿using Gomez.Core.Cache.LinqCache.Containers;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Linq;
using Z.EntityFramework.Plus;

namespace Gomez.Core.Ef.Cache.Db.Containers.EntityFramework
{
#pragma warning disable S3881 // "IDisposable" should be implemented correctly, it is implemented correctely
    public class DbCacheContainer : Container
#pragma warning restore S3881 // "IDisposable" should be implemented correctly
    {
        public string ContainerId { get; set; }
        private DbContext _db;

        public DbCacheContainer(DbContext db)
        {
            SupportsDurationInvalidation = true;
            _db = db;
        }

        public DbCacheContainer(DbContext db, string containerId) : this(db)
        {
            ContainerId = containerId;
        }

        public override void Clear()
        {
            _db.Set<DbCacheEntry>().Where(x => x.ContainerId == ContainerId).Delete();
        }

        public override void Delete(string key)
        {
            _db.Set<DbCacheEntry>().Where(x => x.ContainerId == ContainerId && x.Key == key).Delete();
        }

        public override bool Get(string key, out object value)
        {
            value = null;
            var entry = _db.Set<DbCacheEntry>().Find(ContainerId, key);
            if (entry != null)
            {
                if (entry.ExpireAt <= DateTime.UtcNow)
                {
                    return false;
                }

                Type outputType = Type.GetType(entry.AssemblyQualifiedName);
                value = JsonConvert.DeserializeObject(entry.JsonValue, outputType);
                return true;
            }

            return false;
        }

        public override void Set(string key, object value)
        {
            Set(key, value, new TimeSpan(0));
        }

        public override void Set(string key, object value, TimeSpan duration)
        {
            var currentDate = DateTime.UtcNow;
            DateTime? expireDate = null;
            if (duration.Ticks != 0)
            {
                expireDate = DateTime.UtcNow.Add(duration);
            }

            string jsonStr = JsonConvert.SerializeObject(value, Formatting.None);
            if (_db.Set<DbCacheEntry>().Where(x => x.ContainerId == ContainerId && x.Key == key).Any())
            {
                _db.Set<DbCacheEntry>().Where(x => x.ContainerId == ContainerId && x.Key == key).Update(x => (new DbCacheEntry
                {
                    JsonValue = jsonStr,
                    UpdatedAt = currentDate,
                    ExpireAt = expireDate
                }));
                return;
            }

            var obj = new DbCacheEntry()
            {
                Key = key,
                JsonValue = jsonStr,
                AssemblyQualifiedName = value.GetType().AssemblyQualifiedName,
                CreatedAt = currentDate,
                ContainerId = ContainerId,
                ExpireAt = expireDate
            };

            _db.Set<DbCacheEntry>().Add(obj);
            _db.SaveChanges();
        }

        public override void CleanExpired()
        {
            var currentDate = DateTime.UtcNow;
            _db.Set<DbCacheEntry>().Where(x => x.ContainerId == ContainerId && x.ExpireAt <= currentDate).Delete();
        }


        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected override void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing && _db != null)
                {
                    _db.Dispose();
                    _db = null;
                }

                disposedValue = true;
            }
        }
        #endregion
    }
}
