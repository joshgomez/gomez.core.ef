﻿using Gomez.Core.Ef.Models.Areas.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using System;
using System.Linq;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace Gomez.Core.Ef.Repositories
{
    public class RefreshTokenRepository<TContext> : DbRepositoryBase<TContext, RefreshToken>
        where TContext : DbContext
    {
        public RefreshTokenRepository(TContext db, IStringLocalizerFactory localizerFactory) : base(db, localizerFactory)
        {
        }

        public async Task<int> RevokeAsync(Guid userId)
        {
            return await _db.Set<RefreshToken>().Where(x => x.UserId == userId).DeleteAsync();
        }

        public async Task<int> DeleteByTokenAsync(string token)
        {
            return await _db.Set<RefreshToken>().Where(x => x.Token == token).DeleteAsync();
        }

        public async Task SaveTokenAsync<TUser>(TUser user, RefreshToken refreshToken) where TUser : ApplicationUser
        {
            _db.Set<RefreshToken>().Add(refreshToken);
            user.LastLoginAt = DateTime.UtcNow;
            await _db.SaveChangesAsync();
        }
    }
}
