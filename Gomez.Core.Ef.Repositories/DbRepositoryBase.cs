﻿using Gomez.Core.Models.Entity;
using Gomez.Core.Models.Exceptions;
using Gomez.Core.Repositories;
using Gomez.Core.Utilities;
using Gomez.Core.ViewModels.Shared;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gomez.Core.Ef.Repositories
{
    public abstract class DbRepositoryBase<TContext> where TContext : DbContext
    {
        protected readonly TContext _db;
        protected readonly IStringLocalizerFactory _localizerFactory;

        protected DbRepositoryBase(TContext db, IStringLocalizerFactory localizerFactory)
        {
            _db = db;
            _localizerFactory = localizerFactory;
        }
    }

    public abstract class DbRepositoryBase<TContext, T> : RepositoryBase<T>
        where TContext : DbContext
        where T : class
    {
        protected readonly TContext _db;

        protected DbRepositoryBase(TContext db, IStringLocalizerFactory localizerFactory) : base(localizerFactory)
        {
            _db = db;
            Query = _db.Set<T>().AsQueryable();
        }

        public Task<Microsoft.EntityFrameworkCore.Storage.IDbContextTransaction> BeginTransactionAsync()
        {
            return _db.Database.BeginTransactionAsync();
        }

        public async Task<T> FindAsync(object id)
        {
            return await _db.Set<T>().FindAsync(id);
        }

        /// <summary>
        /// Save changes, used mostly for internal changes.
        /// </summary>
        /// <param name="yes"></param>
        /// <returns></returns>
        public async Task<int> SaveChangesAsync(bool yes)
        {
            if (yes)
            {
                return await _db.SaveChangesAsync();
            }

            return -1;
        }

        public async Task<int> DeleteAsync(T item, bool saveChanges = true)
        {
            if(item == null)
            {
                return -1;
            }

            _db.Set<T>().Remove(item);
            return await SaveChangesAsync(saveChanges);
        }

        public async Task<int> DeleteAsync(bool saveChanges = true, params T[] items)
        {
            if(items == null || items.Length == 0)
            {
                return -1;
            }

            _db.Set<T>().RemoveRange(items);
            return await SaveChangesAsync(saveChanges);
        }

        /// <summary>
        /// Should not be called directly. Require inserting property implmentation.
        /// </summary>
        /// <typeparam name="TPropType"></typeparam>
        /// <param name="property"></param>
        /// <param name="value"></param>
        /// <param name="saveChanges"></param>
        /// <returns></returns>
        protected async Task<int> SetSinglePropertyAsync<TPropType>(IPropertyBaseModel property, TPropType value, bool saveChanges = true, byte[] rowVersion = null)
        {
            if (property != null)
            {
                if (rowVersion != null && !ByteUtility.ArrayCompare(rowVersion, property.RowVersion))
                {
                    throw new ConcurrencyException(property.Key);
                }

                var typeNum = PropertyBaseModel.GetPropertyTypeNum(typeof(TPropType));
                if (property.TypeNum != typeNum)
                {
                    throw new InvalidCastException(nameof(value));
                }

                property.Value = value;
            }

            return await SaveChangesAsync(saveChanges);
        }

        protected async Task<SimplePropertyDtoCollection> DtoPropsToSimpleCollection<TEntityId>(IQueryable<PropertyDto<TEntityId>> propertyQuery) where TEntityId : struct
        {
            var properties = await propertyQuery
                .Select(x => new SimplePropertyDto() { Key = x.Key, TypeNum = x.TypeNum, Value = x.Value, RowVersion = x.RowVersion }).ToArrayAsync();
            return new SimplePropertyDtoCollection(properties);
        }

        protected SimplePropertyDtoCollection PropsToSimpleCollection<TProperty>(ICollection<TProperty> propertyCollection) where TProperty : PropertyBaseModel
        {
            var properties = propertyCollection.Select(x => new SimplePropertyDto() { Key = x.Key, TypeNum = x.TypeNum, Value = x.Value, RowVersion = x.RowVersion });
            return new SimplePropertyDtoCollection(properties);
        }

        /// <summary>
        /// Should not be called directly. Require inserting property implmentation.
        /// </summary>
        /// <param name="properties"></param>
        /// <param name="saveChanges"></param>
        /// <param name="pairs"></param>
        /// <returns></returns>
        protected async Task SetInternalAsync(IPropertyCollection properties, bool saveChanges, params (string key, object value, byte[] rowVersion)[] pairs)
        {
            foreach (var (key, value, rowVersion) in pairs)
            {
                if (value == null)
                {
                    properties.DeleteProperty(key);
                    continue;
                }

                if (rowVersion != null && !ByteUtility.ArrayCompare(properties.GetRowVersion(key), rowVersion))
                {
                    throw new ConcurrencyException(nameof(key));
                }

                properties.SetProperty(value.GetType(), key, value);
            }

            if (saveChanges)
                await _db.SaveChangesAsync();
        }
    }
}
