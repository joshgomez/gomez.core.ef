﻿using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace Gomez.Core.Ef.Localization.SqlLocalizer.DbStringLocalizer
{
    public class SqlStringLocalizer : IStringLocalizer
    {
        private readonly Dictionary<string, string> _localizations;

        private readonly FallbackHandler _handler;
        private readonly string _resourceKey;

        public SqlStringLocalizer(Dictionary<string, string> localizations, FallbackHandler handler, string resourceKey)
        {
            _localizations = localizations;
            _handler = handler;
            _resourceKey = resourceKey;
        }

        public LocalizedString this[string name]
        {
            get
            {
                var text = GetText(name, out bool notSucceed);
                return new LocalizedString(name, text, notSucceed);
            }
        }

        public LocalizedString this[string name, params object[] arguments]
        {
            get
            {
                if(arguments == null || arguments.Length == 0)
                {
                    return this[name];
                }

                var text = GetText(name, out bool notSucceed);
                if (!string.IsNullOrWhiteSpace(text))
                {
                    text = string.Format(text, arguments);
                }

                return new LocalizedString(name, text, notSucceed);
            }
        }

        public IEnumerable<LocalizedString> GetAllStrings(bool includeParentCultures)
        {
            foreach (var item in _localizations)
            {
                yield return new LocalizedString(item.Key, item.Value);
            }
        }

        public IStringLocalizer WithCulture(CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        private string GetText(string key, out bool notSucceed)
        {
            var culture = CultureInfo.CurrentCulture.ToString();
            string computedKey = $"{key}.{culture}";

            if (_localizations.TryGetValue(computedKey, out string result))
            {
                notSucceed = false;
                return result;
            }
            else
            {
                notSucceed = true;
                if (_handler.AddNewLocalizedItem(key, culture, _resourceKey))
                {
                    _localizations.Add(computedKey, computedKey);
                    return computedKey;
                }

                return _resourceKey + "." + computedKey;
            }
        }



    }
}
