﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace Gomez.Core.Ef.Localization.SqlLocalizer.DbStringLocalizer
{
    public class SqlStringLocalizerFactory : IStringExtendedLocalizerFactory
    {
        private readonly FallbackHandler _handler;
        private readonly LocalizationModelContext _context;
        private static readonly ConcurrentDictionary<string, IStringLocalizer> _resourceLocalizations = new ConcurrentDictionary<string, IStringLocalizer>();

        public SqlStringLocalizerFactory(LocalizationModelContext context, FallbackHandler handler)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _handler = handler ?? throw new ArgumentNullException(nameof(handler));
        }

        public IStringLocalizer Create(Type resourceSource)
        {
            if (_resourceLocalizations.Keys.Contains(resourceSource.FullName))
            {
                return _resourceLocalizations[resourceSource.FullName];
            }

            var collection = GetAllFromDatabaseForResource(resourceSource.FullName);
            _handler.AddResourceFromFile(resourceSource, collection);
            var sqlStringLocalizer = new SqlStringLocalizer(collection, _handler, resourceSource.FullName);
            return _resourceLocalizations.GetOrAdd(resourceSource.FullName, sqlStringLocalizer);
        }

        public IStringLocalizer Create(string baseName, string location)
        {

            if (_resourceLocalizations.Keys.Contains(baseName + location))
            {
                return _resourceLocalizations[baseName + location];
            }

            var sqlStringLocalizer = new SqlStringLocalizer(GetAllFromDatabaseForResource(baseName + location), _handler, baseName + location);
            return _resourceLocalizations.GetOrAdd(baseName + location, sqlStringLocalizer);
        }

        public void ResetCache()
        {
            _resourceLocalizations.Clear();

            lock (_context)
            {
                _context.DetachAllEntities();
            }
        }

        public void ResetCache(Type resourceSource)
        {
            _resourceLocalizations.TryRemove(resourceSource.FullName, out _);

            lock (_context)
            {
                _context.DetachAllEntities();
            }
        }

        private Dictionary<string, string> GetAllFromDatabaseForResource(string resourceKey)
        {
            lock (_context)
            {
                return _context.LocalizationRecords.Where(data => data.ResourceKey == resourceKey)
                    .ToDictionary(kvp => (kvp.Key + "." + kvp.LocalizationCulture), kvp => kvp.Text);
            }
        }

        public IList GetImportHistory()
        {
            lock (_context)
            {
                return _context.ImportHistoryDbSet.ToList();
            }
        }

        public IList GetExportHistory()
        {
            lock (_context)
            {
                return _context.ExportHistoryDbSet.ToList();
            }
        }

        public IList GetLocalizationData(string reason = "export")
        {
            lock (_context)
            {
                _context.ExportHistoryDbSet.Add(new ExportHistory { Reason = reason, Exported = DateTime.UtcNow });
                _context.SaveChanges();

                return _context.LocalizationRecords.ToList();
            }
        }

        public IList GetLocalizationData(DateTime from, string culture = null, string reason = "export")
        {
            lock (_context)
            {
                _context.ExportHistoryDbSet.Add(new ExportHistory { Reason = reason, Exported = DateTime.UtcNow });
                _context.SaveChanges();

                if (culture != null)
                {
                    return _context.LocalizationRecords.Where(item =>
                            EF.Property<DateTime>(item, "UpdatedTimestamp") > from &&
                            item.LocalizationCulture == culture)
                        .ToList();
                }

                return _context.LocalizationRecords
                    .Where(item => EF.Property<DateTime>(item, "UpdatedTimestamp") > from).ToList();
            }
        }


        public void UpdatetLocalizationData(List<LocalizationRecord> data, string information)
        {
            lock (_context)
            {
                _context.DetachAllEntities();
                _context.UpdateRange(data);
                _context.ImportHistoryDbSet.Add(new ImportHistory { Information = information, Imported = DateTime.UtcNow });
                _context.SaveChanges();
            }
        }

        public void AddNewLocalizationData(List<LocalizationRecord> data, string information)
        {
            lock (_context)
            {
                _context.DetachAllEntities();
                _context.AddRange(data);
                _context.ImportHistoryDbSet.Add(new ImportHistory { Information = information, Imported = DateTime.UtcNow });
                _context.SaveChanges();
            }
        }
    }
}
