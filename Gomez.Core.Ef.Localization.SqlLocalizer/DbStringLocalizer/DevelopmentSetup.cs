﻿
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Options;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Resources;

namespace Gomez.Core.Ef.Localization.SqlLocalizer.DbStringLocalizer
{
    // >dotnet ef migrations add LocalizationMigration
    public class FallbackHandler
    {
        private readonly LocalizationModelContext _context;
        private readonly IOptions<SqlLocalizationOptions> _options;
        private readonly IOptions<RequestLocalizationOptions> _requestLocalizationOptions;

        public FallbackHandler(
           LocalizationModelContext context,
           IOptions<SqlLocalizationOptions> localizationOptions,
           IOptions<RequestLocalizationOptions> requestLocalizationOptions)
        {
            _options = localizationOptions;
            _context = context;
            _requestLocalizationOptions = requestLocalizationOptions;
        }

        public void AddResourceFromFile(Type resourceSource, Dictionary<string, string> collection)
        {
            foreach (var ci in _requestLocalizationOptions.Value.SupportedCultures)
            {
                AddResourceFromFile(ci, resourceSource, collection);
            }
        }

        private void AddResourceFromFile(CultureInfo ci, Type resourceSource, Dictionary<string, string> collection)
        {
            if (!_options.Value.CreateNewRecordOnDefault)
            {
                return;
            }

            var records = new List<LocalizationRecord>();
            string culture = ci.ToString();

            try
            {
                var rm = new ResourceManager(resourceSource.FullName, resourceSource.Assembly);
                var resourceSet = rm.GetResourceSet(ci, true, true);
                foreach (DictionaryEntry entry in resourceSet)
                {
                    string key = entry.Key.ToString();
                    string computedKey = $"{key}.{culture}";
                    if (!collection.ContainsKey(computedKey))
                    {
                        string value = entry.Value.ToString();
                        collection.Add(computedKey, value);
                        records.Add(new LocalizationRecord()
                        {
                            LocalizationCulture = culture,
                            Key = key,
                            Text = value,
                            ResourceKey = resourceSource.FullName
                        });
                    }
                }
            }
            catch (MissingManifestResourceException)
            {
                //Not a resource file.
            }

            if (records.Count > 0)
            {
                lock (_context)
                {
                    _context.LocalizationRecords.AddRange(records);
                    _context.SaveChanges();
                }
            }
        }

        public bool AddNewLocalizedItem(string key, string culture, string resourceKey)
        {
            if (_options.Value.CreateNewRecordOnDefault && _requestLocalizationOptions.Value.SupportedCultures.Contains(new System.Globalization.CultureInfo(culture)))
            {
                string computedKey = $"{key}.{culture}";

                LocalizationRecord localizationRecord = new LocalizationRecord()
                {
                    LocalizationCulture = culture,
                    Key = key,
                    Text = computedKey,
                    ResourceKey = resourceKey
                };

                lock (_context)
                {
                    //AlternateKey
                    var existingRecord = _context.LocalizationRecords.Where(x =>
                        x.Key == localizationRecord.Key &&
                        x.LocalizationCulture == localizationRecord.LocalizationCulture &&
                        x.ResourceKey == localizationRecord.ResourceKey)
                        .FirstOrDefault();

                    if(existingRecord == null)
                    {
                        _context.LocalizationRecords.Add(localizationRecord);
                    }
                    else
                    {
                        existingRecord.Text = computedKey;
                    }

                    _context.SaveChanges();
                }

                return true;
            }

            return false;
        }
    }
}