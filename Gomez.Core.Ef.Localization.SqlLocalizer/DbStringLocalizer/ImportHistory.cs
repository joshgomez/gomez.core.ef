﻿using System;

namespace Gomez.Core.Ef.Localization.SqlLocalizer.DbStringLocalizer
{
    public class ImportHistory
    {
        public long Id { get; set; }

        public DateTime Imported { get; set; }

        public string Information { get; set; }
    }
}
