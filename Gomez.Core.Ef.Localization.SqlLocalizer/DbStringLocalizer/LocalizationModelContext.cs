﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Linq;

namespace Gomez.Core.Ef.Localization.SqlLocalizer.DbStringLocalizer
{
    // >dotnet ef migrations add LocalizationMigration
    public class LocalizationModelContext : DbContext
    {
        private readonly string _schema;

        public LocalizationModelContext(DbContextOptions<LocalizationModelContext> options, IOptions<SqlContextOptions> contextOptions) : base(options)
        {
            _schema = contextOptions.Value.SqlSchemaName;
        }

        public DbSet<LocalizationRecord> LocalizationRecords { get; set; }
        public DbSet<ExportHistory> ExportHistoryDbSet { get; set; }
        public DbSet<ImportHistory> ImportHistoryDbSet { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            if (!string.IsNullOrEmpty(_schema))
                modelBuilder.HasDefaultSchema(_schema);
            modelBuilder.Entity<LocalizationRecord>().HasKey(m => m.Id);
            modelBuilder.Entity<LocalizationRecord>().HasAlternateKey(c => new { c.Key, c.LocalizationCulture, c.ResourceKey });

            // shadow properties
            modelBuilder.Entity<LocalizationRecord>().Property<DateTime>("UpdatedTimestamp");

            modelBuilder.Entity<ExportHistory>().HasKey(m => m.Id);

            modelBuilder.Entity<ImportHistory>().HasKey(m => m.Id);

            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            ChangeTracker.DetectChanges();
            UpdateUpdatedProperty<LocalizationRecord>();
            return base.SaveChanges();
        }

        private void UpdateUpdatedProperty<T>() where T : class
        {
            var modifiedSourceInfo =
                ChangeTracker.Entries<T>()
                    .Where(e => e.State == EntityState.Added || e.State == EntityState.Modified);

            foreach (var entry in modifiedSourceInfo)
            {
                entry.Property("UpdatedTimestamp").CurrentValue = DateTime.UtcNow;
            }
        }

        public void DetachAllEntities()
        {
            var changedEntriesCopy = ChangeTracker.Entries().ToList();
            foreach (var entity in changedEntriesCopy)
            {
                Entry(entity.Entity).State = EntityState.Detached;
            }
        }
    }
}