﻿namespace Gomez.Core.Ef.Localization.SqlLocalizer
{
    public class SqlLocalizationOptions
    {
        /// <summary>
        /// Creates a new item in the SQL database if the resource is not found
        /// </summary>
        public bool CreateNewRecordOnDefault { get; set; }

        /// <summary>
        /// You can set the required properties to set, get, display the different localization
        /// </summary>
        /// <param name="createNewRecordOnDefault"></param>
        public void UseSettings(bool createNewRecordOnDefault)
        {
            CreateNewRecordOnDefault = createNewRecordOnDefault;
        }
    }
}
