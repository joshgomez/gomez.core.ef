﻿using Gomez.Core.Ef.Models.Areas.Identity;
using System.Collections.Generic;
using System.Net;
using System.Security.Claims;

namespace Gomez.Core.Ef.Web.Services
{
    public interface IJwtTokenService<in TUser> where TUser : ApplicationUser
    {
        string BuildToken(TUser user, IEnumerable<Claim> additionalClaims = null);
        RefreshToken GenerateRefreshToken(TUser user, IPAddress ip);
        ClaimsPrincipal GetPrincipalFromExpiredToken(string token);
    }
}