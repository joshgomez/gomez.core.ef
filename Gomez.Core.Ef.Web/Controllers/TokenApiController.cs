﻿using Gomez.Core.Ef.Models.Areas.Identity;
using Gomez.Core.Ef.Repositories;
using Gomez.Core.Ef.Web.Helpers;
using Gomez.Core.Ef.Web.Services;
using Gomez.Core.Utilities;
using Gomez.Core.ViewModels.Areas.Identity;
using Gomez.Core.Web.Extensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Gomez.Core.Ef.Web.Controllers
{
    public abstract class TokenApiController<TContext, TUser, TRole, TController> : MainApiController<TContext, TUser, TController>
        where TContext : DbContext
        where TUser : ApplicationUser
        where TRole : ApplicationRole
        where TController : ControllerBase
    {
        private readonly IJwtTokenService<TUser> _tokenService;
        private readonly SignInManager<TUser> _signInManager;
        private readonly RoleManager<TRole> _roleManager;

        protected TokenApiController(TContext db, UserManager<TUser> um, 
            IStringLocalizerFactory localizerFactory,
            IJwtTokenService<TUser> tokenService,
            RoleManager<TRole> rm,
            SignInManager<TUser> sim, ILogger<TController> logger) : base(db, um, localizerFactory,logger)
        {
            _tokenService = tokenService;
            _signInManager = sim;
            _roleManager = rm;
        }            

        protected virtual async Task<IActionResult> Login(LoginForm vm)
        {
            if (!ModelState.IsValid)
            {
                var error = ModelState.Values.SelectMany(x => x.Errors).FirstOrDefault()?.ErrorMessage;
                return BadRequest(error);
            }

            var result = await _signInManager.PasswordSignInAsync(vm.Email, vm.Password, vm.RememberMe, true);
            if (result.Succeeded)
            {
                var user = await _userManager.FindByEmailAsync(vm.Email);
                vm.ReturnUrl ??= Url.Content("~/");
                var refreshToken = _tokenService.GenerateRefreshToken(user,this.Request.HttpContext.Connection.RemoteIpAddress);
                var repo = new RefreshTokenRepository<TContext>(_db, _localizerFactory);
                await repo.SaveTokenAsync(user, refreshToken);

                return Ok(new { token = await GenerateTokenAsync(user), refreshToken = new 
                    {   
                        token = refreshToken.Token, 
                        expires = refreshToken.Expires 
                    }, 
                    returnUrl = vm.ReturnUrl });
            }

            if (result.IsLockedOut)
            {
                return BadRequest("Token.Login.IsLockedOut");
            }

            return BadRequest("Token.Login.Invalid");
        }

        protected virtual async Task<IActionResult> Refresh(QueryRefreshTokenViewModel query)
        {
            var principal = _tokenService.GetPrincipalFromExpiredToken(query.Token);
            var userId = principal.GetUserId<Guid>();

            var tokenCache = await _db.Set<RefreshToken>().SingleOrDefaultAsync(x => x.Token == query.RefreshToken && x.UserId == userId);
            if (tokenCache == null)
            {
                return BadRequest("RefreshToken.Invalid");
            }

            if(DateTime.UtcNow > tokenCache.Expires)
            {
                return BadRequest("RefreshToken.Expired");
            }

            if(tokenCache.RemoteIpAddress != null && tokenCache.RemoteIpAddress.Length > 0)
            {
                var currentIp = this.Request.HttpContext.Connection.RemoteIpAddress.GetAddressBytes();
                if (!ByteUtility.ArrayCompare(tokenCache.RemoteIpAddress, currentIp))
                {
                    return BadRequest("RefreshToken.HackingDetected");
                }
            }

            var repo = new RefreshTokenRepository<TContext>(_db, _localizerFactory);
            await repo.DeleteByTokenAsync(query.RefreshToken);

            var user = await _userManager.FindByIdAsync(userId.ToString());
            var newRefreshToken = _tokenService.GenerateRefreshToken(user, this.Request.HttpContext.Connection.RemoteIpAddress);
            await repo.SaveTokenAsync(user, newRefreshToken);

            return Ok(new
            {
                token = await GenerateTokenAsync(user),
                refreshToken = new
                {
                    token = newRefreshToken.Token,
                    expires = newRefreshToken.Expires
                }
            });
        }

        // Generates a token from the token service and returns it as a string
        protected async virtual Task<string> GenerateTokenAsync(TUser user)
        {
            var claims = await GetValidClaimsAsync(user);
            var token = _tokenService.BuildToken(user, claims);
            return token;
        }

        private async Task<IList<Claim>> GetValidClaimsAsync(TUser user)
        {
            return await IdentityHelper.GetValidClaimsAsync(user, _userManager, _roleManager);
        }

        protected async Task<IActionResult> Revoke()
        {
            var userId = User.GetUserId<Guid>();
            var repo = new RefreshTokenRepository<TContext>(_db, _localizerFactory);
            return Ok(await repo.RevokeAsync(userId));
        }
    }
}
