﻿using Gomez.Core.Ef.Models.Areas.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;

namespace Gomez.Core.Ef.Web.Controllers
{
    [ApiController]
    public abstract class MainApiController<TContext, TUser, TController> : ControllerBase
        where TContext : DbContext
        where TUser : ApplicationUser
        where TController : ControllerBase
    {
        protected readonly TContext _db;
        protected readonly UserManager<TUser> _userManager;
        protected readonly IStringLocalizerFactory _localizerFactory;
        protected readonly ILogger<TController> _logger;

        protected MainApiController(TContext db, IStringLocalizerFactory localizerFactory) : base()
        {
            _db = db;
            _localizerFactory = localizerFactory;
        }

        protected MainApiController(TContext db, UserManager<TUser> um, IStringLocalizerFactory localizerFactory) : this(db, localizerFactory)
        {
            _userManager = um;
        }

        protected MainApiController(TContext db, 
            UserManager<TUser> um, 
            IStringLocalizerFactory localizerFactory, 
            ILogger<TController> logger) : this(db, um, localizerFactory)
        {
            _logger = logger;
        }
    }
}
