﻿using Gomez.Core.Ef.Models.Areas.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;

namespace Gomez.Core.Ef.Web.Controllers
{
    public abstract class MainController<TContext, TUser, TController> : Controller
        where TContext : DbContext
        where TUser : ApplicationUser
        where TController : Controller
    {
        protected readonly TContext _db;
        protected readonly UserManager<TUser> _userManager;
        protected readonly IStringLocalizerFactory _localizerFactory;
        protected readonly ILogger<TController> _logger;

        protected MainController(TContext db, IStringLocalizerFactory localizerFactory) : base()
        {
            _db = db;
            _localizerFactory = localizerFactory;
        }

        protected MainController(TContext db, UserManager<TUser> um, IStringLocalizerFactory localizerFactory) : this(db, localizerFactory)
        {
            _userManager = um;
        }

        protected MainController(TContext db,
            UserManager<TUser> um,
            IStringLocalizerFactory localizerFactory,
            ILogger<TController> logger) : this(db, um, localizerFactory)
        {
            _logger = logger;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var requestCulture = Request.HttpContext.Features.Get<IRequestCultureFeature>();
            ViewData["Language"] = requestCulture.RequestCulture.UICulture.TwoLetterISOLanguageName;
            base.OnActionExecuting(context);
        }
    }
}
