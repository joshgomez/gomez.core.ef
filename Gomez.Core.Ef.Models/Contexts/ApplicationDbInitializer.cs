﻿using Gomez.Core.Ef.Models.Areas.Identity;
using Gomez.Core.Utilities;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Gomez.Core.Ef.Models.Contexts
{
    public class ApplicationDbInitializer<TUser> where TUser : ApplicationUser, new()
    {
        private readonly Dictionary<string,(string password,string[] roles)> _users;
        private readonly UserManager<TUser> _userManager;

        public ApplicationDbInitializer(UserManager<TUser> userManager)
        {
            _users = new Dictionary<string, (string, string[])>();
            _userManager = userManager;
        }

        protected void AddUser(string email, string password, params string[] roles)
        {
            _users.Add(email, (password, roles));
        }

        public void SeedUsers()
        {
            if(_users.Count == 0)
            {
                return;
            }

            AsyncPump.Run(async () =>
            {
                await AddUsersAsync();
            });
        }

        private async Task AddUsersAsync()
        {
            foreach (var item in _users)
            {
                await AddUserAsync(item.Key, item.Value.password, item.Value.roles);
            }
        }

        private async Task AddUserAsync(string email, string password, params string[] roles)
        {
            if (await _userManager.FindByNameAsync(email) == null)
            {
                var user = new TUser
                {
                    UserName = email,
                    Email = email
                };

                IdentityResult result = await _userManager.CreateAsync(user, password);
                if (roles.Length > 0 && result.Succeeded)
                {
                    await _userManager.AddToRolesAsync(user, roles);
                }
            }
        }
    }
}
