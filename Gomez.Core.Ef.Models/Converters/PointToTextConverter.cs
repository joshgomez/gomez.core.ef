﻿using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using NetTopologySuite.Geometries;

namespace Gomez.Core.Ef.Models.Converters
{
    public static class PointToTextConverter
    {
        public static ValueConverter<Point, string> Instance { get; private set; } = Create();

        private static ValueConverter<Point, string> Create()
        {
            return new ValueConverter<Point, string>(
                v => v.ToText(),
                v => ToPoint(v)
            );
        }

        private static Point ToPoint(string value)
        {
            if(string.IsNullOrEmpty(value))
            {
                return new Point(0, 0);
            }

            var reader = new NetTopologySuite.IO.WKTReader();
            var geometry = reader.Read(value);
            return new Point(geometry.Coordinate);
        }
    }
}
