﻿using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using NetTopologySuite.Geometries;

namespace Gomez.Core.Ef.Models.Converters
{
    public static class PointToBinaryConverter
    {
        public static ValueConverter<Point, byte[]> Instance { get; private set; } = Create();

        private static ValueConverter<Point, byte[]> Create()
        {
            return new ValueConverter<Point, byte[]>(
                v => v.ToBinary(),
                v => ToPoint(v)
            );
        }

        private static Point ToPoint(byte[] value)
        {
            if(value == null || value.Length == 0)
            {
                return new Point(0, 0);
            }

            var reader = new NetTopologySuite.IO.WKBReader();
            var geometry = reader.Read(value);
            return new Point(geometry.Coordinate);
        }
    }
}
