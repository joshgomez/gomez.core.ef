﻿using Gomez.Core.Models.Entity;
using Microsoft.AspNetCore.Identity;
using System;

namespace Gomez.Core.Ef.Models.Areas.Identity
{
    public class ApplicationUser : IdentityUser<Guid>, IEntityBase
    {
        public ApplicationUser()
        {

        }

        public DateTime CreatedAt { get; set; } = DateTime.UtcNow;
        public DateTime? UpdatedAt { get; set; }
        public DateTime? DeletedAt { get; set; }
        public DateTime? LastLoginAt { get; set; }
        public int SortingOrder { get; set; } = 0;
    }
}
