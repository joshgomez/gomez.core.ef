﻿using Gomez.Core.Models.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.ComponentModel.DataAnnotations;
using System.Net;

namespace Gomez.Core.Ef.Models.Areas.Identity
{
    public class RefreshTokenConfiguration : IEntityTypeConfiguration<RefreshToken>
    {
        public void Configure(EntityTypeBuilder<RefreshToken> builder)
        {
            builder.HasAlternateKey(x => new { x.Token, x.UserId });
            builder.HasIndex(x => x.Expires);
        }
    }

    public class RefreshToken : RefreshToken<Guid>
    {
        public RefreshToken()
        {

        }

        public RefreshToken(string token, DateTime expires, Guid userId, IPAddress remoteIpAddress) : base(token, expires, userId, remoteIpAddress)
        {
        }

        public RefreshToken(string token, DateTime expires, Guid userId, byte[] remoteIpAddress) : base(token, expires, userId, remoteIpAddress)
        {
        }
    }

    public abstract class RefreshToken<T> : EntityBase
    {
        [Key]
        [MaxLength(255)]
        public string Token { get; set; }
        public DateTime Expires { get; set; }
        public T UserId { get; set; }

        [MaxLength(16)]
        public byte[] RemoteIpAddress { get; set; }

        public bool Active => DateTime.UtcNow <= Expires;

        protected RefreshToken()
        {

        }

        private RefreshToken(string token, DateTime expires, T userId)
        {
            Token = token;
            Expires = expires;
            UserId = userId;
        }

        protected RefreshToken(string token, DateTime expires, T userId, IPAddress remoteIpAddress) : this(token,expires,userId)
        {
            RemoteIpAddress = remoteIpAddress.GetAddressBytes();
        }

        protected RefreshToken(string token, DateTime expires, T userId, byte[] remoteIpAddress) : this(token, expires, userId)
        {
            RemoteIpAddress = remoteIpAddress;
        }
    }
}