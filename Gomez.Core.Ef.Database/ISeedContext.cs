﻿namespace Gomez.Core.Ef.Database
{
    public interface ISeedContext
    {
        void Seed();
    }
}