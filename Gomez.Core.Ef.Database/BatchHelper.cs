﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Gomez.Core.Ef.Database
{
    public static class BatchHelper
    {
        public static async Task<BatchHelper<T>> CreateAsync<T>(IQueryable<T> source, int pageIndex, int pageSize)
        {
            return await BatchHelper<T>.CreateAsync(source, pageIndex, pageSize);
        }
    }

    public class BatchHelper<T>
    {
        public IQueryable<T> Source { get; private set; }
        public int Count { get; private set; }
        public int PageIndex { get; private set; }
        public int TotalPages => PageSize > 0 ? (int)Math.Ceiling(Count / (double)PageSize) : 0;
        public int PageSize { get; set; }

        protected BatchHelper(IQueryable<T> source, int count, int pageIndex, int pageSize)
        {
            Source = source;
            Count = count;
            PageIndex = pageIndex;
            PageSize = pageSize;
        }

        public bool HasPreviousPage
        {
            get
            {
                return (PageIndex > 1);
            }
        }

        public bool HasNextPage
        {
            get
            {
                return (PageIndex < TotalPages);
            }
        }

        public static async Task<BatchHelper<T>> CreateAsync(IQueryable<T> source, int pageIndex, int pageSize)
        {
            var count = await source.CountAsync();
            return new BatchHelper<T>(source, count, pageIndex, pageSize);
        }

        public IQueryable<T> GetBatch()
        {
            return GetBatch(PageIndex);
        }

        public IQueryable<T> GetBatch(int page)
        {
            page = Math.Max(1, page);
            page = Math.Min(TotalPages, page);

            return Source.Skip((page - 1) * PageSize).Take(PageSize);
        }

        public IQueryable<T> GetNextBatch()
        {
            if (HasNextPage)
            {
                PageIndex++;
            }

            return Source.Skip((PageIndex - 1) * PageSize).Take(PageSize);
        }

        public IQueryable<T> GetPreviousBatch()
        {
            if (HasPreviousPage)
            {
                PageIndex--;
            }

            return Source.Skip((PageIndex - 1) * PageSize).Take(PageSize);
        }
    }
}
