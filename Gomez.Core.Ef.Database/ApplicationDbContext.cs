﻿using Gomez.Core.Ef.Database.Models;
using Gomez.Core.Ef.Models.Areas.Identity;
using Gomez.Core.Models.Exceptions;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using Z.EntityFramework.Plus;

namespace Gomez.Core.Ef.Database
{
    public abstract class ApplicationDbContext<TUser, TRole, TKey> : IdentityDbContext<TUser, TRole, TKey>
        where TUser : IdentityUser<TKey>
        where TRole : IdentityRole<TKey>
        where TKey : IEquatable<TKey>
    {
        protected ApplicationDbContext(DbContextOptions<ApplicationDbContext<TUser, TRole, TKey>> options)
            : base(options)
        {

        }

        protected ApplicationDbContext(DbContextOptions options)
            : base(options)
        {

        }

        public DbSet<RefreshToken> RefreshTokens { get; set; }

        public Audit SetAndGetAudit<TEntry, TProperty>(TEntry entry = null, TProperty property = null, string userId = null)
            where TEntry : BaseAuditEntry
            where TProperty : BaseAuditEntryProperty
        {
            var audit = new Audit();
            if (!String.IsNullOrEmpty(userId))
            {
                audit.CreatedBy = userId;
            }

            audit.Configuration.AuditEntryFactory = args => entry ?? (TEntry)Activator.CreateInstance(typeof(TEntry));
            audit.Configuration.AuditEntryPropertyFactory = args => property ?? (TProperty)Activator.CreateInstance(typeof(TProperty));
            return audit;
        }

        public bool IsConcurrencyException(Exception ex)
        {
            if (ex is DbUpdateConcurrencyException || ex is ConcurrencyException)
            {
                return true;
            }

            return false;
        }
    }
}
