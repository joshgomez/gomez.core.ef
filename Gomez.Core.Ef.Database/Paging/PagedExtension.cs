﻿using System.Linq;
using System.Threading.Tasks;
using E = Gomez.Core.Models.Entity;
using EfE = Gomez.Core.Ef.Models.Entity;

namespace Gomez.Core.Ef.Database.Paging
{
    public static class PagedExtension
    {
        public static E.IPagedResult<T> GetPaged<T>(this IOrderedQueryable<T> query,
                                         int page, int pageSize) where T : class
        {
            return E.PagedExtension.GetPaged<T>(query, page, pageSize);
        }

        public static async Task<E.IPagedResult<T>> GetPagedAsync<T>(this IOrderedQueryable<T> query,
                                 int page, int pageSize) where T : class
        {
            return await EfE.PagedExtension.GetPagedAsync<T>(query, page, pageSize);
        }
    }
}
